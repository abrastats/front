# Abrastats UI

React front end of Abrastats

## Install

1. `git clone git@gitlab.com:abrastats/front.git`
2. `cd front && npm install`
3. `npm run start`

## Contribute

To contribute to the project, fork the project in your own repository, create a new branch called `feature/description` or `fix/description` and open a merge request for it on the main repo. 

If you need to do so, don't forget to create the same branch for the scripts and the API respositories.

- If you add a new page, add it in `./src/pages`, create a Route for it in `./src/components/navigation/Router.tsx`, and add the dedicated Link to the `./src/components/navigation/Header.tsx`
- Use the same style templating as the other pages
- Create a mock for your datas in `./src/helpers/mocks`, the mock must match the expected return type you'll create in the API

Contact Clonescody#1164 on the [Abracadabra Discord](https://discord.gg/pbmftrJ2) for further informations.


## Todo

1. Finish the migration from [https://gitlab.com/Clonescody/mim-flip](https://gitlab.com/Clonescody/mim-flip)