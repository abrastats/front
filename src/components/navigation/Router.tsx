import React from 'react'
import { Route, Routes } from 'react-router'
import LandingPage from '../../pages/LandingPage'
import CauldronPage from '../../pages/CauldronPage'
import FeesPage from '../../pages/FeesPage'
import TVLPage from '../../pages/TVLPage'
import StakingPage from '../../pages/StakingPage'
import MIMPage from '../../pages/MIMPage'
import TreasuryPage from '../../pages/TreasuryPage'
import ToolsPage from '../../pages/ToolsPage'
import AboutPage from '../../pages/AboutPage'

const MyRouter = (): JSX.Element => (
  <Routes>
    <Route path='/' element={<LandingPage />} />
    <Route path='/cauldrons/:address' element={<CauldronPage />} />
    <Route path='/fees' element={<FeesPage />} />
    <Route path='/tvl' element={<TVLPage />} />
    <Route path='/staking' element={<StakingPage />} />
    <Route path='/mim' element={<MIMPage />} />
    <Route path='/treasury' element={<TreasuryPage />} />
    <Route path='/tools' element={<ToolsPage />} />
    <Route path='/about' element={<AboutPage />} />
  </Routes>
)

export default MyRouter
