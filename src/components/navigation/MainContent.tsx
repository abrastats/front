import { Box } from '@chakra-ui/react'
import React, { ReactNode } from 'react'

type MainContentProps = {
  children: ReactNode
}

const MainContent: React.FunctionComponent<MainContentProps> = ({ children }): JSX.Element => (
  <Box w={'85%'} maxHeight='100vh' overflowY='scroll'>
    {children}
  </Box>
)
export default MainContent
