import '../../styles/navigation/Header.css'
import React from 'react'
import { Link as RouterLink } from 'react-router-dom'
import { Flex, Image, Link, LinkBox, Text, Icon } from '@chakra-ui/react'
import {
  AiOutlineEye,
  AiOutlineLock,
  AiOutlinePercentage,
  AiOutlineQuestionCircle,
} from 'react-icons/ai'
import { BsGraphUp } from 'react-icons/bs'
import { GiTwoCoins, GiOpenChest } from 'react-icons/gi'
import { BiWrench } from 'react-icons/bi'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import WizardEmote from '../../images/WizardEmote.png'
import colors from '../../helpers/colors'

const Header = (): JSX.Element => {
  return (
    <Flex
      flexDirection='column'
      w={'15%'}
      minWidth={'150px'}
      h={'100%'}
      p='1%'
      bg={colors.primaryLight}
      overflowY='scroll'
    >
      <LinkBox>
        <RouterLink to='/'>
          <Flex flexDirection='row' justifyContent='center' alignItems='center'>
            <Image width={100} height={130} objectFit='cover' src={WizardEmote} alt='Wizard logo' />
          </Flex>
          <Text textAlign={'center'} color='white'>
            Abrastats
          </Text>
        </RouterLink>
      </LinkBox>
      <Flex
        flexDirection='column'
        justifyContent='flex-start'
        alignItems='flex-start'
        height='80%'
        mt='5%'
      >
        <Link
          display='flex'
          flexDirection='row'
          alignItems='center'
          as={RouterLink}
          layerStyle='navigationBarButton'
          to='/'
        >
          <AiOutlineEye />
          <Text ml='5px'>Overview</Text>
        </Link>
        <Link
          display='flex'
          flexDirection='row'
          alignItems='center'
          as={RouterLink}
          layerStyle='navigationBarButton'
          to='/fees'
        >
          <BsGraphUp />
          <Text ml='5px'>Fees</Text>
        </Link>
        <Link
          display='flex'
          flexDirection='row'
          alignItems='center'
          as={RouterLink}
          layerStyle='navigationBarButton'
          to='/tvl'
        >
          <AiOutlineLock />
          <Text ml='5px'>TVL</Text>
        </Link>
        <Link
          display='flex'
          flexDirection='row'
          alignItems='center'
          as={RouterLink}
          layerStyle='navigationBarButton'
          to='/staking'
        >
          <AiOutlinePercentage />
          <Text ml='5px'>Staking</Text>
        </Link>
        <Link
          display='flex'
          flexDirection='row'
          alignItems='center'
          as={RouterLink}
          layerStyle='navigationBarButton'
          to='/mim'
        >
          <GiTwoCoins />
          <Text ml='5px'>MIM</Text>
        </Link>
        <Link
          display='flex'
          flexDirection='row'
          alignItems='center'
          as={RouterLink}
          layerStyle='navigationBarButton'
          to='/treasury'
        >
          <GiOpenChest />
          <Text ml='5px'>Treasury</Text>
        </Link>
        <Link
          display='flex'
          flexDirection='row'
          alignItems='center'
          as={RouterLink}
          layerStyle='navigationBarButton'
          to='/tools'
        >
          <BiWrench />
          <Text ml='5px'>Tools</Text>
        </Link>
        <Link
          display='flex'
          flexDirection='row'
          alignItems='center'
          as={RouterLink}
          layerStyle='navigationBarButton'
          to='/about'
        >
          <AiOutlineQuestionCircle />
          <Text ml='5px'>About</Text>
        </Link>
      </Flex>
      <Flex
        flexDirection='column'
        justifyContent='flex-start'
        alignItems='flex-start'
        borderTopColor='lightgray'
        borderTopWidth='1px'
        pt='2%'
      >
        <Link layerStyle='navigationBarExternalLink' href='https://abracadabra.money/' isExternal>
          Abracadabra <ExternalLinkIcon h={'24px'} />
        </Link>
        <Link layerStyle='navigationBarExternalLink' href='https://discord.gg/mim' isExternal>
          Discord <ExternalLinkIcon h={'24px'} />
        </Link>
        <Link
          layerStyle='navigationBarExternalLink'
          href='https://twitter.com/MIM_Spell'
          isExternal
        >
          Twitter <ExternalLinkIcon h={'24px'} />
        </Link>
        <Link
          layerStyle='navigationBarExternalLink'
          href='https://docs.abracadabra.money/'
          isExternal
        >
          Docs <ExternalLinkIcon h={'24px'} />
        </Link>
      </Flex>
    </Flex>
  )
}

export default Header
