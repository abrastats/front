import React from 'react'
import NumberFormat from 'react-number-format'
import { Box, Flex, Text } from '@chakra-ui/react'
import colors from '../../helpers/colors'

type ValuesArrayItem = {
  label: string
  value: string
}

type ValuesBoxProps = {
  maxWidth: string
  values: Array<ValuesArrayItem>
  title: string
  precision?: number
}

const ValuesBox = ({ title, values, maxWidth, precision = 3 }: ValuesBoxProps): JSX.Element => {
  return (
    <Flex
      flexDirection='column'
      minW='280px'
      maxW={maxWidth}
      alignItems='center'
      p='2%'
      borderWidth='1px'
      borderRadius='10px'
      mt='1%'
      mb='1%'
      bg={colors.primaryLight}
    >
      <Text mb='2%' color='white'>
        {title}
      </Text>
      <Flex flexDirection='column' justifyContent='center' alignItems='center' h='100%'>
        {values.length > 1 ? (
          values.map((item) => (
            <Box key={item.label}>
              <NumberFormat
                color='white'
                suffix={` ${item.label}`}
                value={parseFloat(item.value).toFixed(precision)}
                displayType='text'
                thousandSeparator
                renderText={(value) => <Text color='white'>{value}</Text>}
              />
            </Box>
          ))
        ) : (
          <NumberFormat
            color='white'
            suffix={` ${values[0].label}`}
            value={parseFloat(values[0].value).toFixed(precision)}
            displayType='text'
            thousandSeparator
            renderText={(value) => <Text color='white'>{value}</Text>}
          />
        )}
      </Flex>
    </Flex>
  )
}

export default ValuesBox
