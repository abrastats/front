import React from 'react'
import { Center, Spinner } from '@chakra-ui/react'

const MyLoader = (): JSX.Element => (
  <Center h='100vh'>
    <Spinner color='#7b79f7' />
  </Center>
)

export default MyLoader
