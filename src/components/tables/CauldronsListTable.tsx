import React, { ChangeEvent, useEffect, useState } from 'react'
import {
  Button,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
  Flex,
  InputLeftElement,
  Input,
  InputGroup,
} from '@chakra-ui/react'
import { Menu, MenuButton, MenuList, MenuItem } from '@chakra-ui/react'
import { ChevronDownIcon } from '@chakra-ui/icons'
import { Checkbox } from '@chakra-ui/react'
import { FiSearch } from 'react-icons/fi'

import { getChainNameFromId, getShortValueString } from '../../helpers/utils'
import { CauldronsListTableProps, ListCauldronProps } from '../../types'
import colors from '../../helpers/colors'

const CauldronsListTable = ({ cauldrons }: CauldronsListTableProps): JSX.Element => {
  const [search, setSearch] = useState('')
  const [showDeprecated, setShowDeprecated] = useState(false)
  const [filteredCauldrons, setFilteredCauldrons] = useState<Array<ListCauldronProps>>(
    cauldrons.filter((cauldron) => !cauldron.isDeprecated),
  )
  const [chainFilters, setChainFilters] = useState<Array<string>>([
    'ETH',
    'BSC',
    'FTM',
    'ARBI',
    'AVAX',
  ])
  const handleSearchChange = (event: ChangeEvent<HTMLInputElement>): void =>
    setSearch(event.target.value)

  useEffect(() => {
    filterCauldrons()
  }, [showDeprecated])

  useEffect(() => {
    if (chainFilters.length === 0) {
      setChainFilters(['ETH', 'BSC', 'FTM', 'ARBI', 'AVAX'])
    } else {
      filterCauldrons()
    }
  }, [chainFilters])

  const filterCauldrons = () => {
    if (!showDeprecated) {
      if (chainFilters.length > 0) {
        setFilteredCauldrons(
          cauldrons.filter(
            (cauldron) =>
              cauldron.isDeprecated === false &&
              chainFilters.includes(getChainNameFromId(cauldron.network)),
          ),
        )
      } else {
        setFilteredCauldrons(cauldrons.filter((cauldron) => cauldron.isDeprecated === false))
      }
    } else {
      if (chainFilters.length > 0) {
        setFilteredCauldrons(
          cauldrons.filter((cauldron) =>
            chainFilters.includes(getChainNameFromId(cauldron.network)),
          ),
        )
      } else {
        setFilteredCauldrons(cauldrons)
      }
    }
  }

  const toggleDeprecatedFilter = () => {
    setShowDeprecated(!showDeprecated)
  }

  const filterWithChain = (chain: string) => {
    if (chainFilters.includes(chain)) {
      setChainFilters(chainFilters.filter((item) => item !== chain))
    } else {
      setChainFilters([...chainFilters, chain])
    }
  }

  return (
    <TableContainer borderWidth='1px' p='2%' bg={colors.primaryLight} borderRadius='20px'>
      <Flex justifyContent='flex-start'>
        <InputGroup maxWidth='600px'>
          <InputLeftElement pointerEvents='none'>
            <FiSearch color='gray' />
          </InputLeftElement>
          <Input
            textColor='white'
            type='text'
            placeholder='Search an asset'
            value={search}
            onChange={handleSearchChange}
          />
        </InputGroup>
        <Flex width='100%' flexDirection='row' justifyContent='flex-end'>
          <Menu closeOnSelect={false}>
            <MenuButton bg={colors.secondary} as={Button} rightIcon={<ChevronDownIcon />}>
              Filters
            </MenuButton>
            <MenuList>
              <MenuItem>
                <Checkbox onChange={() => toggleDeprecatedFilter()} isChecked={showDeprecated}>
                  Show deprecated
                </Checkbox>
              </MenuItem>
              <MenuItem>
                <Checkbox
                  isChecked={chainFilters.includes('ETH')}
                  onChange={() => filterWithChain('ETH')}
                  value='ETH'
                >
                  Ethereum
                </Checkbox>
              </MenuItem>
              <MenuItem>
                <Checkbox
                  isChecked={chainFilters.includes('BSC')}
                  onChange={() => filterWithChain('BSC')}
                  value='BSC'
                >
                  Binance Smart Chain
                </Checkbox>
              </MenuItem>
              <MenuItem>
                <Checkbox
                  isChecked={chainFilters.includes('FTM')}
                  onChange={() => filterWithChain('FTM')}
                  value='FTM'
                >
                  Fantom
                </Checkbox>
              </MenuItem>
              <MenuItem>
                <Checkbox
                  isChecked={chainFilters.includes('ARBI')}
                  onChange={() => filterWithChain('ARBI')}
                  value='ARBI'
                >
                  Arbitrum
                </Checkbox>
              </MenuItem>
              <MenuItem>
                <Checkbox
                  isChecked={chainFilters.includes('AVAX')}
                  onChange={() => filterWithChain('AVAX')}
                  value='AVAX'
                >
                  Avalanche
                </Checkbox>
              </MenuItem>
            </MenuList>
          </Menu>
        </Flex>
      </Flex>
      <Table variant='simple'>
        <Thead>
          <Tr>
            <Th color='white'>Chain</Th>
            <Th color='white'>Name</Th>
            <Th color='white' isNumeric>
              TVL
            </Th>
            <Th color='white' isNumeric>
              Borrow fee
            </Th>
            <Th color='white' isNumeric>
              Interest
            </Th>
            <Th color='white' isNumeric>
              MIM borrowed
            </Th>
            <Th color='white' isNumeric>
              MIM available
            </Th>
            <Th color='white' isNumeric>
              Withdrawable amount
            </Th>
          </Tr>
        </Thead>
        <Tbody>
          {filteredCauldrons.map(
            (cauldron) =>
              ((search !== '' && cauldron.name.toLowerCase().includes(search.toLowerCase())) ||
                search === '') && (
                <Tr key={cauldron.address}>
                  <Td color='white'>{getChainNameFromId(cauldron.network)}</Td>
                  <Td color='white'>{cauldron.name}</Td>
                  <Td color='white' isNumeric>
                    {getShortValueString(cauldron.tvl)}
                  </Td>
                  <Td color='white' isNumeric>
                    {cauldron.borrowFee / 1000}%
                  </Td>
                  <Td color='white' isNumeric>
                    {(cauldron.interest / 100000000).toFixed(1)}%
                  </Td>
                  <Td color='white' isNumeric>
                    {getShortValueString(cauldron.mimBorrowed)}
                  </Td>
                  <Td color='white' isNumeric>
                    {getShortValueString(cauldron.mimAvailable)}
                  </Td>
                  <Td color='white' isNumeric>
                    {getShortValueString(cauldron.withdrawableAmount)}
                  </Td>
                </Tr>
              ),
          )}
        </Tbody>
      </Table>
    </TableContainer>
  )
}

export default CauldronsListTable
