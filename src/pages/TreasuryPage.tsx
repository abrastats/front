import React from 'react'
import { Box, Flex, Text } from '@chakra-ui/react'
import MyLoader from '../components/common/MyLoader'
import ValuesBox from '../components/common/ValuesBox'

const TreasuryPage = (): JSX.Element => {
  const isLoading = false
  if (isLoading) {
    return <MyLoader />
  }
  return (
    <Flex flexDirection='column' layerStyle='container'>
      <Text color='white' layerStyle='pageTitle'>
        Treasury page
      </Text>
      <Flex justifyContent='space-around' flexWrap='wrap'>
        <ValuesBox
          maxWidth='44%'
          title='Treasury value'
          values={[
            { label: '$', value: '420040200' },
            { label: 'ETH', value: '9860' },
            { label: 'BTC', value: '650' },
          ]}
          precision={0}
        />
        <ValuesBox
          maxWidth='45%'
          title='Curve veCRV'
          values={[
            { label: '$ locked', value: '12421412' },
            { label: 'voting power', value: '32324244' },
            { label: 'mCRV supply', value: '5451616' },
          ]}
          precision={0}
        />
      </Flex>
    </Flex>
  )
}

export default TreasuryPage
