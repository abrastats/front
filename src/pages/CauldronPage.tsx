import { Flex, Text } from '@chakra-ui/react'
import React from 'react'
import MyLoader from '../components/common/MyLoader'

const CauldronPage = (): JSX.Element => {
  const isLoading = false
  if (isLoading) {
    return <MyLoader />
  }
  return (
    <Flex flexDirection='column' layerStyle='container'>
      <Text color='white' layerStyle='pageTitle'>
        Cauldron page
      </Text>
    </Flex>
  )
}

export default CauldronPage
