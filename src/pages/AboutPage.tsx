import React from 'react'
import { Flex, Text } from '@chakra-ui/react'
import MyLoader from '../components/common/MyLoader'

const AboutPage = (): JSX.Element => {
  const isLoading = true
  if (isLoading) {
    return <MyLoader />
  }
  return (
    <Flex flexDirection='column' layerStyle='container'>
      <Text color='white' layerStyle='pageTitle'>
        About page
      </Text>
    </Flex>
  )
}

export default AboutPage
