import React from 'react'
import { Flex, Text } from '@chakra-ui/react'
import MyLoader from '../components/common/MyLoader'

const StakingPage = (): JSX.Element => {
  const isLoading = false
  if (isLoading) {
    return <MyLoader />
  }
  return (
    <Flex flexDirection='column' layerStyle='container'>
      <Text color='white' layerStyle='pageTitle'>
        Staking page
      </Text>
    </Flex>
  )
}

export default StakingPage
