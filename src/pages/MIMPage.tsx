import React from 'react'
import { Flex, Text } from '@chakra-ui/react'
import MyLoader from '../components/common/MyLoader'

const MIMPage = (): JSX.Element => {
  const isLoading = false
  if (isLoading) {
    return <MyLoader />
  }
  return (
    <Flex flexDirection='column' layerStyle='container'>
      <Text color='white' layerStyle='pageTitle'>
        MIM page
      </Text>
    </Flex>
  )
}

export default MIMPage
