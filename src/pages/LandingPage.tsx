import React from 'react'
import { Flex, Text } from '@chakra-ui/react'
import MyLoader from '../components/common/MyLoader'
import ValuesBox from '../components/common/ValuesBox'
import CauldronsListMock from '../helpers/mocks/CauldronsListMock'
import CauldronsListTable from '../components/tables/CauldronsListTable'

const LandingPage = (): JSX.Element => {
  const isLoading = false
  const cauldronsList = CauldronsListMock
  if (isLoading) {
    return <MyLoader />
  }
  return (
    <Flex flexDirection='column' layerStyle='container'>
      <Flex justifyContent='space-around' flexWrap='wrap' mb='1%'>
        <ValuesBox
          maxWidth='20%'
          title='Total Value Locked'
          values={[{ label: '$', value: '420040200' }]}
          precision={0}
        />
        <ValuesBox
          maxWidth='20%'
          title='MIM supply'
          values={[
            { label: 'MIM borrowed', value: '12421412' },
            { label: 'MIM available', value: '32324244' },
          ]}
          precision={0}
        />
        <ValuesBox
          maxWidth='20%'
          title='Revenues last week'
          values={[
            { label: '$ of SPELL emitted', value: '12421412' },
            { label: 'MIM of revenues', value: '32324244' },
          ]}
          precision={0}
        />
        <ValuesBox
          maxWidth='20%'
          title='SPELL overview'
          values={[
            { label: '$ market cap', value: '12421412' },
            { label: 'SPELL in circulation', value: '32324244' },
          ]}
          precision={0}
        />
      </Flex>
      <Text color='white' mb='1%'>
        Cauldrons list
      </Text>
      <CauldronsListTable cauldrons={cauldronsList} />
    </Flex>
  )
}

export default LandingPage
