import { extendTheme } from '@chakra-ui/react'

export default extendTheme({
  layerStyles: {
    container: {
      pt: '2%',
      pr: '1%',
      pl: '1%',
      pb: '1%',
    },
    navigationBarButton: {
      textColor: 'white',
      p: '2%',
      mb: '1%',
    },
    navigationBarExternalLink: {
      textColor: 'white',
    },
    pageTitle: {
      mb: '2%',
    },
  },
  fonts: {
    heading: 'Inter',
    body: 'Inter',
  },
})
