export const ETH_RPC_URL = 'https://rpc.flashbots.net'
export const AVALANCHE_RPC_URL = 'https://api.avax.network/ext/bc/C/rpc'
export const FANTOM_RPC_URL = 'https://rpc.ftm.tools'
export const ARBITRUM_RPC_URL = 'https://arb1.arbitrum.io/rpc'
export const BINANCE_RPC_URL = 'https://bsc-dataseed1.binance.org:443'

export const getChainNameFromId = (id: number): string => {
  switch (id) {
    default:
    case 1:
      return 'ETH'
    case 56:
      return 'BSC'
    case 250:
      return 'FTM'
    case 42161:
      return 'ARBI'
    case 43114:
      return 'AVAX'
  }
}

export const getRPCForChain = (id: number): string => {
  switch (id) {
    default:
    case 1:
      return ETH_RPC_URL
    case 56:
      return BINANCE_RPC_URL
    case 250:
      return FANTOM_RPC_URL
    case 42161:
      return ARBITRUM_RPC_URL
    case 43114:
      return AVALANCHE_RPC_URL
  }
}

export const getShortValueString = (value: number, decimals = 1): string => {
  let dividedValue = `${value}`
  let label = ''
  if (value >= 1000) {
    if (value >= 10000) {
      if (value >= 100000) {
        if (value >= 1000000) {
          if (value >= 10000000) {
            if (value >= 100000000) {
              if (value >= 1000000000) {
                dividedValue = `${value / 1000000000}`
                label = 'bn'
              } else {
                dividedValue = `${value / 1000000}`
                label = 'm'
              }
            } else {
              dividedValue = `${value / 1000000}`
              label = 'm'
            }
          } else {
            dividedValue = `${value / 1000000}`
            label = 'm'
          }
        } else {
          dividedValue = `${value / 1000}`
          label = 'k'
        }
      } else {
        dividedValue = `${value / 1000}`
        label = 'k'
      }
    } else {
      dividedValue = `${value / 1000}`
      label = 'k'
    }
  }
  return `${parseFloat(dividedValue).toFixed(decimals)}${label}`
}
