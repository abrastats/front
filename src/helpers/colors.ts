export default {
  primary: '#221b47',
  primaryLight: '#312B55',
  primaryLighter: '#6a5eaa',
  secondary: '#7b79f7',
}
