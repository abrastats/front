import React from 'react'
import { Flex } from '@chakra-ui/react'
import Header from './components/navigation/Header'
import MainContent from './components/navigation/MainContent'
import colors from './helpers/colors'
import MyRouter from './components/navigation/Router'

const App = (): JSX.Element => {
  return (
    <Flex
      bg={colors.primary}
      flexDirection='row'
      justifyContent='flex-start'
      alignItems='flex-start'
      height='full'
    >
      <Header />
      <MainContent>
        <MyRouter />
      </MainContent>
    </Flex>
  )
}

export default App
