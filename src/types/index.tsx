export type Collateral = {
  name: string
  address: string
}

export type ListCauldronProps = {
  name: string
  address: string
  network: number
  assetType: number
  liquidationFee: number
  borrowFee: number
  interest: number
  collateral: Collateral
  degenBox: string
  withdrawableAmount: number
  isDeprecated: boolean
  tvl: number
  mimBorrowed: number
  mimAvailable: number
}

export type CauldronsListTableProps = {
  cauldrons: Array<ListCauldronProps>
}
